Soal 1 Membuat Database
create databases myshop;

Soal 2 Membuat Table di Dalam Database
Tabel user
MariaDB [(none)]> use myshop
Database changed
MariaDB [myshop]> create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> pasword varchar(255)
    -> );
Tabel categories
create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
-> );
Tabel items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(10),
    -> stock int(10),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );
Soal 3 Memasukkan Data pada Table
Insert user
insert into users(name,email,pasword) values("John Doe","john@doe.com","john123"),("Jane doe","jane@doe.com","jenita123");

Inset category
insert into categories(name) values("gedget"),("cloth"),("men"),("women"),("branded");

Inset item
insert into items(name,description,price,stock,category_id) values("samsung b50","hape keren dari merek sumsang",4000000,100,1),("uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

Soal 4 Mengambil Data dari Database

a.Mengambil data users
select id,name,email from users;
b.Mengambil data items
select * from items where price>1000000;
select * from items where name like '%watch';

c.Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = category_id;
Soal 5 Mengubah Data dari Database
 update items set price=200000 where id=2;

